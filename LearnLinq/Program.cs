using System;
using System.Collections.Generic;
using System.Linq;

using LearnLinq.Models;

namespace LearnLinq
{
    class Program
    {
        private static List<Author> authors = new List<Author>
        {
            new Author{ Id = 1, FirstName = "Mahendro", LastName = "Suhendra", Age = 20 },
            new Author{ Id = 2, FirstName = "Alat", LastName = "Saja", Age = 24 },
            new Author{ Id = 3, FirstName = "Keluarga", LastName = "Them", Age = 20 },
            new Author{ Id = 4, FirstName = "Silang", LastName = "Uligr", Age = 20 },
            new Author{ Id = 5, FirstName = "Gajah", LastName = "Asetat", Age = 19 },
            new Author{ Id = 6, FirstName = "Mada", LastName = "Polin", Age = 24 },
            new Author{ Id = 7, FirstName = "Suka", LastName = "Munma", Age = 22 },
            new Author{ Id = 8, FirstName = "Bunga", LastName = "Eryam", Age = 25 },
            new Author{ Id = 9, FirstName = "Makan", LastName = "Eulne", Age = 26 },
            new Author{ Id = 10, FirstName = "Minum", LastName = "Yupen", Age = 26 },
            new Author{ Id = 11, FirstName = "Sederhana", LastName = "Weyua", Age = 25 },
            new Author{ Id = 12, FirstName = "Restoran", LastName = "Puntaf", Age = 21 },
            new Author{ Id = 13, FirstName = "Istimewa", LastName = "Luapf", Age = 18 },
            new Author{ Id = 14, FirstName = "Mengapa", LastName = "Muegna", Age = 19 },
            new Author{ Id = 15, FirstName = "Kamu", LastName = "Yuren", Age = 24 }
        };

        private static List<Book> books = new List<Book>
        {
            new Book{ Id = 1, Title = "Benalu Menunggumu", Description = "Kekaisaran Benalu" },
            new Book{ Id = 2, Title = "Ayah Menunggumu", Description = "Kekaisaran Ayah" },
            new Book{ Id = 3, Title = "Ibu Menunggumu", Description = "Kekaisaran Ibu" },
            new Book{ Id = 4, Title = "Gajah Menunggumu", Description = "Kekaisaran Gajah" },
            new Book{ Id = 5, Title = "Sapi Menunggumu" },
            new Book{ Id = 6, Title = "Kambing Menunggumu" },
            new Book{ Id = 7, Title = "Betina Menunggumu", Description = "Kekaisaran Betina" },
            new Book{ Id = 8, Title = "Jantan Menunggumu", Description = "Kekaisaran Jantan" },
            new Book{ Id = 9, Title = "Sehingga Menunggumu" },
            new Book{ Id = 10, Title = "Butuh Menunggumu", Description = "Kekaisaran Butuh" },
        };

        private static void Main(string[] args)
        {
            SetupModelRelations();

            // RunLinqMethod();
            RunLinqQuery();
        }

        private static void RunLinqMethod()
        {
            var authorWithId12 = authors.Find(author => author.Id == 12);
            var authorWithId13 = authors.First(author => author.Id == 13);

            var authorsWithId12Or13 = authors.Where(author => author.Id == 12 || author.Id == 13);

            var booksWithNullDescription = books.Where(book => book.Description == null);

            Console.WriteLine("Done");
        }

        private static void RunLinqQuery()
        {
            var authorWithId12 = from author in authors
                                 where author.Id == 12
                                 select author;

            var authorWithId13 = from author in authors
                                 where author.Id == 13
                                 select author;

            var authorsWithId12Or13 = from author in authors
                                      where (author.Id == 12 || author.Id == 13)
                                      select author;

            var booksWithNullDescription = from book in books
                                           where book.Description == null
                                           select book;

            var booksWithNotNullDescriptionCount = (from book in books
                                                    where book.Description != null
                                                    select book).Count();

            var sortedAuthorsByAgeAscendingAndNameDescending = from author in authors
                                                               orderby author.Age ascending, (author.FirstName + author.LastName) descending
                                                               select new
                                                               {
                                                                   Age = author.Age,
                                                                   FullName = $"{author.FirstName} {author.LastName}"
                                                               };

            var sortedAuthorsGroupedByAge = from author in (
                                                from author in authors
                                                orderby author.Age
                                                select new
                                                {
                                                    Age = author.Age,
                                                    FullName = $"{author.FirstName} {author.LastName}"
                                                })
                                            group author by author.Age;

            var joinAuthorAndBook = from author in authors
                                    join book in books
                                    on author.Id equals book.Author.Id
                                    select new
                                    {
                                        Author = author,
                                        Book = book
                                    };

            Console.WriteLine("Done");
        }

        private static void SetupModelRelations()
        {
            AuthorWriteBook(1, 1);
            AuthorWriteBook(2, 2);
            AuthorWriteBook(2, 3);
            AuthorWriteBook(2, 4);
            AuthorWriteBook(4, 5);
            AuthorWriteBook(5, 6);
            AuthorWriteBook(6, 7);
            AuthorWriteBook(8, 8);
            AuthorWriteBook(9, 9);
            AuthorWriteBook(9, 10);
        }

        private static void AuthorWriteBook(int authorId, int bookId)
        {
            var author = authors.First(author => author.Id == authorId);
            var book = books.First(book => book.Id == bookId);

            if (author != null && book != null)
            {
                book.Author = author;
            }
        }
    }
}

using App.DataAccess;
using App.DataAccess.Models;
using App.Web.Services;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Swashbuckle.AspNetCore;
using Swashbuckle.AspNetCore.SwaggerUI;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });
            services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<ApplicationDbContext>();

            ConfigureContextModelServices(services);
            Task configureIdentityRoles = ConfigureIdentityRoles(services);
            Task configureAdminRole = ConfigureAdminRole(services);

            services.AddSwaggerGen();
        }

        private void ConfigureContextModelServices(IServiceCollection services)
        {
            services.AddScoped<IAuthorService, AuthorService>();
            services.AddScoped<IBookService, BookService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
        }

        private async Task ConfigureIdentityRoles(IServiceCollection services)
        {
            IServiceProvider provider = services.BuildServiceProvider();

            var roleManager = provider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = provider.GetRequiredService<UserManager<ApplicationUser>>();

            string[] roleNames = new string[] {
                AuthenticationService.Role.ADMIN,
                AuthenticationService.Role.GUEST
            };

            foreach (var roleName in roleNames)
            {
                if (!await roleManager.RoleExistsAsync(roleName))
                {
                    await roleManager.CreateAsync(new IdentityRole(roleName));
                }
            }
        }

        private async Task ConfigureAdminRole(IServiceCollection services)
        {
            IServiceProvider provider = services.BuildServiceProvider();

            var userManager = provider.GetRequiredService<UserManager<ApplicationUser>>();
            var authenticationService = provider.GetRequiredService<IAuthenticationService>();

            string adminEmail = Configuration["ApplicationAdmin:Email"];
            string adminPassword = Configuration["ApplicationAdmin:Password"];

            if (await userManager.FindByEmailAsync(adminEmail) == null)
            {
                var registerResult = await authenticationService.RegisterAdminUser(adminEmail, adminPassword);
                var adminUser = await authenticationService.GetUserByEmail(adminEmail);

                await userManager.AddToRoleAsync(adminUser, AuthenticationService.Role.ADMIN);
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Web API v1 Test");
            });
        }
    }
}

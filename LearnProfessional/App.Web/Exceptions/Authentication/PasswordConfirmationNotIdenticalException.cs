using System;

namespace App.Web.Exceptions.Authentication
{
    public class PasswordConfirmationNotIdenticalException : Exception
    {
        public PasswordConfirmationNotIdenticalException(string message) : base(message)
        {

        }
    }
}
using App.DataAccess.Models;

namespace App.Web.Services
{
    public interface IAuthorService : IServiceBase<Author>
    {

    }
}
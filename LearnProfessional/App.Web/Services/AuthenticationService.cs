using App.DataAccess;
using App.DataAccess.Models;
using App.Web.Exceptions.Authentication;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;

namespace App.Web.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        public struct Role
        {
            public const string ADMIN = "Admin";
            public const string GUEST = "Guest";
        }

        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AuthenticationService(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        public async Task<SignInResult> LoginUser(string email, string password, bool rememberMe = false)
        {
            var result = await _signInManager.PasswordSignInAsync(email, password, rememberMe, false);

            var user = await _userManager.FindByEmailAsync(email);

            return result;
        }

        public async Task<ApplicationUser> GetUserByAuth(ClaimsPrincipal claims)
        {
            return await _userManager.GetUserAsync(claims);
        }

        public async Task<ApplicationUser> GetUserByEmail(string email)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<IdentityResult> RegisterAdminUser(string email, string password)
        {
            var registerResult = await RegisterUser(email, password, password, Role.ADMIN);

            return registerResult;
        }

        public async Task<IdentityResult> RegisterUser(string email, string password, string passwordConfirmation, string role = AuthenticationService.Role.GUEST)
        {
            if (password != passwordConfirmation)
            {
                throw new PasswordConfirmationNotIdenticalException("Password confirmation doesn't match");
            }

            var user = new ApplicationUser
            {
                Email = email,
                UserName = email
            };

            var result = await _userManager.CreateAsync(user, password);

            user = await _userManager.FindByEmailAsync(email);
            await _userManager.AddToRoleAsync(user, role);

            return result;
        }

        public async Task LogoutUser()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<List<string>> GetUserRoles(ApplicationUser user)
        {
            return await _userManager.GetRolesAsync(user) as List<string>;
        }

        public async Task<bool> UserHasAdminRole(ApplicationUser user)
        {
            return (await GetUserRoles(user)).Where(role => role == Role.ADMIN).Count() > 0;
        }
    }
}
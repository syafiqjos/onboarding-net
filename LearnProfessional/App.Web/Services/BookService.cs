using App.DataAccess;
using App.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace App.Web.Services
{
    public class BookService : ServiceBase<Book>, IBookService
    {
        public BookService(ApplicationDbContext db) : base(db) { }

        public override DbSet<Book> GetDbSet() => _db.Books;

        public override void AlterModel(Book existingModel, Book newModel)
        {
            if (!string.IsNullOrEmpty(newModel.Name)) existingModel.Name = newModel.Name;
            if (!string.IsNullOrEmpty(newModel.Description)) existingModel.Description = newModel.Description;

            if (newModel.Price != null) existingModel.Price = newModel.Price;
            if (newModel.AbsoluteStock != null) existingModel.AbsoluteStock = newModel.AbsoluteStock;
            if (newModel.TotalPage != null) existingModel.TotalPage = newModel.TotalPage;
            if (newModel.Width != null) existingModel.Width = newModel.Width;
            if (newModel.Weight != null) existingModel.Weight = newModel.Weight;
        }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Web.Services
{
    public interface IServiceBase<TModel> where TModel : class
    {
        Task<List<TModel>> GetAll();

        Task<TModel> GetById(int id);

        Task<TModel> Create(TModel model);

        Task<bool> DeleteById(int id);

        Task<TModel> UpdateById(int id, TModel model);
    }
}
using App.DataAccess.Models;

using Microsoft.AspNetCore.Identity;

using System.Collections.Generic;
using System.Threading.Tasks;
using System.Security.Claims;

namespace App.Web.Services
{
    public interface IAuthenticationService
    {
        Task<SignInResult> LoginUser(string email, string password, bool rememberMe = false);
        Task<ApplicationUser> GetUserByAuth(ClaimsPrincipal claims);
        Task<ApplicationUser> GetUserByEmail(string email);
        Task<IdentityResult> RegisterAdminUser(string email, string password);
        Task<IdentityResult> RegisterUser(string email, string password, string passwordConfirmation, string role = AuthenticationService.Role.GUEST);
        Task LogoutUser();
        Task<List<string>> GetUserRoles(ApplicationUser user);
        Task<bool> UserHasAdminRole(ApplicationUser user);
    }
}
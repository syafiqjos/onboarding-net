using App.DataAccess;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Web.Services
{
    public abstract class ServiceBase<TModel> : IServiceBase<TModel> where TModel : class
    {
        protected readonly ApplicationDbContext _db;

        public ServiceBase(ApplicationDbContext db)
        {
            _db = db;
        }

        public virtual DbSet<TModel> GetDbSet()
        {
            throw new NotImplementedException("Override first to get DbSet<TModel> from database context.");
        }

        public virtual void AlterModel(TModel existingModel, TModel newModel)
        {
            throw new NotImplementedException("Possibly error, override to alter existing model from database.");
        }

        public async Task<List<TModel>> GetAll()
        {
            return await GetDbSet().ToListAsync<TModel>();
        }

        public async Task<TModel> GetById(int id)
        {
            return await GetDbSet().FindAsync(id);
        }

        public async Task<TModel> Create(TModel model)
        {
            await GetDbSet().AddAsync(model);
            await _db.SaveChangesAsync();

            return model;
        }

        public async Task<bool> DeleteById(int id)
        {
            var model = await GetById(id);
            if (model == null) return false;

            GetDbSet().Remove(model);
            await _db.SaveChangesAsync();

            return true;
        }

        public async Task<TModel> UpdateById(int id, TModel newModel)
        {
            var existingModel = await GetById(id);
            if (existingModel == null) return null;

            AlterModel(existingModel, newModel);

            _db.Update(existingModel);

            await _db.SaveChangesAsync();

            return existingModel;
        }
    }
}
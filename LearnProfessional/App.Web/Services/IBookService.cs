using App.DataAccess.Models;

namespace App.Web.Services
{
    public interface IBookService : IServiceBase<Book>
    {

    }
}
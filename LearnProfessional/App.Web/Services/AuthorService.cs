using App.DataAccess;
using App.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace App.Web.Services
{
    public class AuthorService : ServiceBase<Author>, IAuthorService
    {
        public AuthorService(ApplicationDbContext db) : base(db) { }

        public override DbSet<Author> GetDbSet() => _db.Authors;

        public override void AlterModel(Author existingModel, Author newModel)
        {
            if (!string.IsNullOrEmpty(newModel.Name)) existingModel.Name = newModel.Name;
            if (!string.IsNullOrEmpty(newModel.Description)) existingModel.Description = newModel.Description;
            if (!string.IsNullOrEmpty(newModel.Country)) existingModel.Country = newModel.Country;
        }
    }
}
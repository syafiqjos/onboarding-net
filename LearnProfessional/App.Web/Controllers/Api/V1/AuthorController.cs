using App.Contracts.Requests.Author;
using App.Contracts.Responses.Author;
using App.DataAccess.Models;
using App.Web.Services;

using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Controllers.Api.V1
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            var authors = await _authorService.GetAll();
            List<AuthorResponse> response = MapResponseList(authors);

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var author = await _authorService.GetById(id);

            if (author == null) return NotFound(author);

            AuthorResponse response = MapResponse(author);

            return Ok(response);
        }

        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] CreateAuthorRequest request)
        {
            var author = new Author
            {
                Name = request.Name,
                Description = request.Description,
                Country = request.Country
            };

            var createdAuthor = await _authorService.Create(author);

            AuthorResponse response = MapResponse(createdAuthor);

            return CreatedAtAction(nameof(GetById), new { id = response.Id }, response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] UpdateAuthorRequest request)
        {
            var author = new Author
            {
                Name = request.Name,
                Description = request.Description,
                Country = request.Country
            };

            var updatedAuthor = await _authorService.UpdateById(id, author);

            AuthorResponse response = MapResponse(updatedAuthor);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _authorService.DeleteById(id);

            return NoContent();
        }

        // Mapping

        private static AuthorResponse MapResponse(Author author)
        {
            return new AuthorResponse
            {
                Id = author.Id,
                Name = author.Name,
                Description = author.Description,
                Country = author.Country
            };
        }

        private static List<AuthorResponse> MapResponseList(List<Author> authors)
        {
            List<AuthorResponse> responses = new List<AuthorResponse>();

            foreach (var author in authors)
            {
                AuthorResponse response = MapResponse(author);
                responses.Add(response);
            }

            return responses;
        }
    }
}
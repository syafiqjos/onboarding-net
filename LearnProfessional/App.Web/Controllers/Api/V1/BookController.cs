using App.Contracts.Requests.Book;
using App.Contracts.Responses.Book;
using App.DataAccess.Models;
using App.Web.Services;

using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Controllers.Api.V1
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    public class BookController : ControllerBase
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            var books = await _bookService.GetAll();
            List<BookResponse> response = MapResponseList(books);

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var book = await _bookService.GetById(id);

            if (book == null) return NotFound(book);

            BookResponse response = MapResponse(book);

            return Ok(response);
        }

        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] CreateBookRequest request)
        {
            var book = new Book
            {
                Name = request.Name,
                AbsoluteStock = request.AbsoluteStock,
                Description = request.Description,
                Price = request.Price,
                TotalPage = request.TotalPage,
                Weight = request.Weight,
                Width = request.Width
            };

            var createdBook = await _bookService.Create(book);

            BookResponse response = MapResponse(createdBook);

            return CreatedAtAction(nameof(GetById), new { id = response.Id }, response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] UpdateBookRequest request)
        {
            var book = new Book
            {
                Name = request.Name,
                Description = request.Description,
                AbsoluteStock = request.AbsoluteStock,
                Price = request.Price,
                TotalPage = request.TotalPage,
                Weight = request.Weight,
                Width = request.Width
            };

            var updatedBook = await _bookService.UpdateById(id, book);

            BookResponse response = MapResponse(updatedBook);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _bookService.DeleteById(id);

            return NoContent();
        }

        // Mapping

        private static BookResponse MapResponse(Book book)
        {
            return new BookResponse
            {
                Id = book.Id,
                Name = book.Name,
                Description = book.Description,
                AbsoluteStock = book.AbsoluteStock ?? 0,
                GoneStock = book.GoneStock ?? 0,
                Price = book.Price ?? 0,
                ReadyStock = book.ReadyStock ?? 0,
                TotalPage = book.TotalPage ?? 0,
                Weight = book.Weight ?? 0,
                Width = book.Width ?? 0
            };
        }

        private static List<BookResponse> MapResponseList(List<Book> books)
        {
            List<BookResponse> responses = new List<BookResponse>();

            foreach (var book in books)
            {
                BookResponse response = MapResponse(book);
                responses.Add(response);
            }

            return responses;
        }
    }
}
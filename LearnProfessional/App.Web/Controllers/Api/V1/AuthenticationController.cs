using App.Contracts.Requests.User;
using App.Contracts.Responses.User;
using App.DataAccess.Models;
using App.Web.Exceptions.Authentication;
using App.Web.Services;

using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Controllers.Api.V1
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterUser([FromBody] RegisterUserRequest request)
        {
            try
            {
                var registerResult = await _authenticationService.RegisterUser(request.Email, request.Password, request.PasswordConfirmation);

                if (!registerResult.Succeeded) return Unauthorized();

                var user = await _authenticationService.GetUserByEmail(request.Email);
                return Ok(user);
            }
            catch (PasswordConfirmationNotIdenticalException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return Unauthorized(e.Message);
            }

        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginUser([FromBody] LoginUserRequest request)
        {
            var loginResult = await _authenticationService.LoginUser(request.Email, request.Password, request.RememberMe);

            if (!loginResult.Succeeded) return Unauthorized();

            var user = await _authenticationService.GetUserByEmail(request.Email);
            return Ok(user);
        }

        [HttpPost("logout")]
        public async Task<IActionResult> LogoutUser()
        {
            await _authenticationService.LogoutUser();

            return NoContent();
        }

        [HttpGet("")]
        public async Task<IActionResult> GetUser()
        {
            var user = await _authenticationService.GetUserByAuth(User);

            return Ok(user);
        }

        [HttpGet("roles")]
        public async Task<IActionResult> GetUserRole()
        {
            var user = await _authenticationService.GetUserByAuth(User);
            var roles = await _authenticationService.GetUserRoles(user);

            return Ok(roles);
        }
    }
}
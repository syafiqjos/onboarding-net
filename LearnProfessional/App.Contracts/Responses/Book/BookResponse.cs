using System.Collections.Generic;

namespace App.Contracts.Responses.Book
{
    public struct BookResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int Price { get; set; }
        public int TotalPage { get; set; }
        public int Weight { get; set; }
        public int Width { get; set; }

        public int AbsoluteStock { get; set; }
        public int ReadyStock { get; set; }
        public int GoneStock { get; set; }
    }
}
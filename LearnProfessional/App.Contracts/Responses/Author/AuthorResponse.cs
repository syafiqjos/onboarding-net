using System.Collections.Generic;

using AuthorModel = App.DataAccess.Models.Author;

namespace App.Contracts.Responses.Author
{
    public struct AuthorResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Country { get; set; }
    }
}
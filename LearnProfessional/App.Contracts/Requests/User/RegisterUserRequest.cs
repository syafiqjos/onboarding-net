using System.ComponentModel.DataAnnotations;

namespace App.Contracts.Requests.User
{
    public struct RegisterUserRequest
    {
        [EmailAddress]
        public string Email { get; set; }

        [MinLength(8)]
        public string Password { get; set; }

        [MinLength(8)]
        public string PasswordConfirmation { get; set; }
    }
}
using System.ComponentModel.DataAnnotations;

namespace App.Contracts.Requests.User
{
    public struct LoginUserRequest
    {
        [EmailAddress]
        public string Email { get; set; }

        [MinLength(8)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
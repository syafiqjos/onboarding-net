using System.ComponentModel.DataAnnotations;

namespace App.Contracts.Requests.Book
{
    public struct CreateBookRequest
    {
        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [Range(0, int.MaxValue)]
        public int Price { get; set; } // in rupiah

        [Range(0, int.MaxValue)]
        public int TotalPage { get; set; } // in integer

        [Range(0, int.MaxValue)]
        public int Weight { get; set; } // in mg

        [Range(0, int.MaxValue)]
        public int Width { get; set; } // in mm

        [Range(0, int.MaxValue)]
        public int AbsoluteStock { get; set; } // jumlah stok buku yang benar benar milik perpustakaan
    }
}
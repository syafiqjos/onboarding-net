using System.ComponentModel.DataAnnotations;

namespace App.Contracts.Requests.Author
{
    public struct UpdateAuthorRequest
    {
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(64)]
        public string Country { get; set; }
    }
}
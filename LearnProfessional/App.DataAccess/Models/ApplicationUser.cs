using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace App.DataAccess.Models
{
    public class ApplicationUser : IdentityUser
    {

    }
}
using System.ComponentModel.DataAnnotations;

namespace App.DataAccess.Models
{
    public class BorrowingDetails
    {
        #region Fields

        [Key]
        public int Id { get; set; }

        public int BorrowedBooksCount { get; set; } // Sudah Pinjam Berapa selama ini
        public int ReturnedBooksCount { get; set; } // Buku yang kembali
        public int UnreturnedBooksCount { get; set; } // Buku yang belum kembali

        #endregion

        #region Relationships

        [Required]
        public ApplicationUser User { get; set; }

        #endregion
    }
}
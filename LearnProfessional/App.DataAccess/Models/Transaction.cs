using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.DataAccess.Models
{
    public class Transaction
    {
        public enum Type
        {
            IsBorrowed,
            HasBookGone,
            HasReturned,
        }

        #region Fields

        [Key]
        public int Id { get; set; }

        [Required]
        public Type Status { get; set; }

        [Required]
        public DateTime BorrowedAt;

        public DateTime ReturnedAt; // default null

        #endregion

        #region Relationships

        [Required]
        public Book Book { get; set; }

        // [Required]
        public ApplicationUser LentBy { get; set; }

        [Required]
        public ApplicationUser BorrowedBy { get; set; }

        #endregion
    }
}
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.DataAccess.Models
{
    public class Author
    {
        #region Fields

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(64)]
        public string Country { get; set; }

        #endregion

        #region Relationships

        public List<Author> Books { get; set; }

        #endregion
    }
}
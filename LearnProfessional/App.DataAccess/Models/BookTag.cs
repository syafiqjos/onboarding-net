using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace App.DataAccess.Models
{
    public class BookTag
    {
        #region Fields

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        #endregion

        #region Relationships

        public List<Book> Books { get; set; }

        #endregion
    }
}
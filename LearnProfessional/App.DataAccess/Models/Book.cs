using System.ComponentModel.DataAnnotations;

namespace App.DataAccess.Models
{
    public class Book
    {
        #region Fields

        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [Range(0, int.MaxValue)]
        public int? Price { get; set; } // in rupiah

        [Range(0, int.MaxValue)]
        public int? TotalPage { get; set; } // in integer

        [Range(0, int.MaxValue)]
        public int? Weight { get; set; } // in mg

        [Range(0, int.MaxValue)]
        public int? Width { get; set; } // in mm

        [Range(0, int.MaxValue)]
        public int? AbsoluteStock { get; set; } // jumlah stok buku yang benar benar milik perpustakaan

        [Range(0, int.MaxValue)]
        public int? ReadyStock { get; set; } // jumlah stok buku yang siap untuk dipinjamkan ke pelanggan

        [Range(0, int.MaxValue)]
        public int? GoneStock { get; set; } // jumlah stok buku yang hilang karena pelanggan

        #endregion

        #region Relationships

        public Author Author { get; set; }

        public Publisher Publisher { get; set; }

        public BookCategory BookCategory { get; set; }

        public BookTag BookTags { get; set; }

        #endregion
    }
}
﻿using System;
using System.Configuration;

namespace LearnEntityFramework
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Learn Entity Framework");
            Console.WriteLine("====== Test Configuration");

            string? appName = ConfigurationManager.AppSettings["AppName"];
            string? connectionString = ConfigurationManager.AppSettings["ConnectionString"];

            Console.WriteLine(appName);
            Console.WriteLine(connectionString);
        }
    }
}
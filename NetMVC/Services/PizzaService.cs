﻿using NetMVC.Models;

namespace NetMVC.Services
{
    public static class PizzaService
    {
        static List<Pizza> Pizzas { get; }
        static int nextId = 3;

        static PizzaService()
        {
            Pizzas = new List<Pizza>()
            {
                new Pizza() { Id = 1, Name = "Pizza Kanak Kanak" },
                new Pizza() { Id = 2, Name = "Pizza Amerika" }
            };
        }

        public static List<Pizza> GetAll() {
            return Pizzas;
        }

        public static Pizza? Get(int id)
        {
            return Pizzas.FirstOrDefault((Pizza pizza) => pizza.Id == id);
        }

        public static void Add(Pizza pizza)
        {
            Pizza newPizza = pizza;
            newPizza.Id = nextId++;

            Pizzas.Add(newPizza);
        }

        public static void Update(int id, Pizza pizza)
        {
            int pizzaIndex = Pizzas.FindIndex((Pizza p) => p.Id == id);

            if (pizzaIndex == -1) return;

            Pizzas[pizzaIndex] = pizza;
        }

        public static void Delete(int id)
        {
            Pizza? pizza = Get(id);

            if (pizza is null) return;

            Pizzas.Remove(pizza);
        }
    }
}

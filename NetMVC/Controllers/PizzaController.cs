﻿using Microsoft.AspNetCore.Mvc;
using NetMVC.Models;
using NetMVC.Services;

namespace NetMVC.Controllers
{
    [Route("[controller]")]
    public class PizzaController : Controller
    {
        private readonly ILogger<PizzaController> _logger;

        public PizzaController(ILogger<PizzaController> logger)
        {
            _logger = logger;
        }

        // API
        [HttpGet]
        public ActionResult<List<Pizza>> GetAll()
        {
            return PizzaService.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<Pizza> Get(int id)
        {
            Pizza? pizza = PizzaService.Get(id);
            if (pizza is null) return NotFound();

            return pizza;
        }

        [HttpPost]
        public IActionResult Store(Pizza pizza)
        {
            PizzaService.Add(pizza);

            return CreatedAtAction(nameof(Store), new { id = pizza.Id }, pizza);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Pizza pizza)
        {
            if (id != pizza.Id) return BadRequest();

            Pizza? existingPizza = PizzaService.Get(id);
            if (existingPizza is null) return NotFound();
            if (id != existingPizza.Id) return BadRequest();

            PizzaService.Update(id, pizza);
            
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Destroy(int id)
        {
            Pizza? existingPizza = PizzaService.Get(id);
            if (existingPizza is null) return NotFound();

            PizzaService.Delete(id);

            return NoContent();
        }
    }
}

using System;
using Pets;
using Xunit;

namespace NewTypesTests;

public class UnitTest1
{
    [Fact]
    public void DogTalkToOwnerReturnsWoof()
    {
		string expected = "Woof - Woof";
		string actual = new Dog().TalkToOwner();

		Assert.Equal(expected, actual);
    }

	[Fact]
	public void CatTalkToOwnerReturnsNyaa()
	{
		string expected = "Nyaa~";
		string actual = new Cat().TalkToOwner();

		Assert.Equal(expected, actual);
	}

	[Fact]
	public void BirdTalkToOwnerReturnsTweet()
	{
		string expected = "Tweet!";
		string actual = new Bird().TalkToOwner();

		Assert.Equal(expected, actual);
	}

	[Theory]
	[InlineData(0)]
	public void CatTalkToOwnerMultipleReturnsNyaa(int times) {
		string expected = "";
		string actual = new Cat().TalkToOwnerMultiple(times);

		Assert.Equal(expected, actual);
	}

	[Theory]
	[InlineData(3)]
	public void CatTalkToOwnerMultipleReturnsNyaa3(int times) {
		string expected = "Nyaa~Nyaa~Nyaa~";
		string actual = new Cat().TalkToOwnerMultiple(times);

		Assert.Equal(expected, actual);
	}

	[Theory]
	[InlineData(0)]
	[InlineData(1)]
	[InlineData(3)]
	public void CatTalkToOwnerMultipleReturnsNyaaNot2(int times) {
		string actual = new Cat().TalkToOwnerMultiple(times);

		Assert.NotEqual("Nyaa~Nyaa~", actual);
	}
}
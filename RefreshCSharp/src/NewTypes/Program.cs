﻿using System;
using Pets;
using System.Collections.Generic;

List<IPet> pets = new List<IPet>() {
	new Dog(),
	new Cat(),
	new Bird()
};

foreach (IPet pet in pets) {
	Console.WriteLine(pet.TalkToOwner());
}
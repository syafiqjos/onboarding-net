﻿using System;

namespace Pets {
	public class Cat: IPet {
		public string TalkToOwner() {
			return "Nyaa~";
		}

		public string TalkToOwnerMultiple(int times) {
			string s = "";
			while (times > 0) {
				s += TalkToOwner();
				times--;
			}
			return s;
		}
	}
}
﻿using System;

namespace Pets {
	public class Dog: IPet {
		public string TalkToOwner() {
			return "Woof - Woof";
		}

		public string TalkToOwnerMultiple(int times) {
			string s = "";
			while (times > 0) {
				s += TalkToOwner();
				times--;
			}
			return s;
		}
	}
}
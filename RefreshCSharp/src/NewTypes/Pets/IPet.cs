﻿using System;

namespace Pets {
	public interface IPet {
		string TalkToOwner();
		string TalkToOwnerMultiple(int times);
	}
}
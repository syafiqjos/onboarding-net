﻿using System;

namespace Pets {
	public class Bird: IPet {
		public string TalkToOwner() {
			return "Tweet!";
		}

		public string TalkToOwnerMultiple(int times) {
			string s = "";
			while (times > 0) {
				s += TalkToOwner();
				times--;
			}
			return s;
		}
	}
}
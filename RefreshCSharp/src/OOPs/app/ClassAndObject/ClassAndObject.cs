using System.Collections.Generic;
using ClassAndObject.Animal;

namespace ClassAndObject.Procedure {
	// public static class AnimalMain {
	public class AnimalMain {
		public static AnimalMain? instance = null;
		public static AnimalMain Instance
		{
			get {
				if (instance == null) {
					instance = new AnimalMain();
				}
				return instance;
			}
		}

		// public void Run() {
		public static void Run() {
			List<AnimalClass> animals = new List<AnimalClass>() {
				new AnimalClass("Gajah", 7, 4, 5, 40),
				new AnimalClass("Sapi", 3, 4, 4, 20),
				new AnimalClass("Ayam", 2, 2, 3, 10)
			};

			foreach(AnimalClass animal in animals) {
				Console.WriteLine(animal.ShowProps());
				Console.WriteLine();
			}

			Console.WriteLine("====== Done! =======\n");

			foreach(AnimalClass animal in animals) {
				Console.WriteLine(animal.ShowProps($"Hello, {animal.Name}!"));
				Console.WriteLine();
			}
		}
	}
}
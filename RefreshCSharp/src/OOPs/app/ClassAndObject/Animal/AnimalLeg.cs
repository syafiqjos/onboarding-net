namespace ClassAndObject.Animal {
	public class AnimalLeg {
		public int Count { get; }

		public AnimalLeg(int count) {
			Count = count;
		}
	}
}
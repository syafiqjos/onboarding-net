namespace ClassAndObject.Animal {
	public class AnimalHead {
		public int Size { get; }

		public AnimalHead(int size) {
			Size = size;
		}
	}
}
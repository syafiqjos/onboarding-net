namespace ClassAndObject.Animal {
	public class AnimalBody {
		public int Size { get; }

		public AnimalBody(int size) {
			Size = size;
		}
	}
}
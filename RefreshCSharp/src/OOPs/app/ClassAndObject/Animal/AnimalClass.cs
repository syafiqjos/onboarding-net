namespace ClassAndObject.Animal {
	public class AnimalClass {
		public string Name { get; }
		
		public AnimalBody Body { get; }
		public AnimalHead Head { get; }
		public AnimalLeg Leg { get; }
		public AnimalTail Tail { get; }

		public AnimalClass(string name, int bodySize, int legCount, int headSize, int tailLength) {
			Name = name;
			Body = new AnimalBody(bodySize);
			Leg = new AnimalLeg(legCount);
			Head = new AnimalHead(headSize);
			Tail = new AnimalTail(tailLength);
		}

		public string ShowProps() {
			return $"Animal Properties\nAnimal Name: {Name},\nBody Size: {Body.Size}m,\nLeg Count: {Leg.Count},\nTail Length: {Tail.Length}cm";
		}

		public string ShowProps(string welcomeText) {
			return $"{welcomeText}\n{ShowProps()}";
		}
	}
}
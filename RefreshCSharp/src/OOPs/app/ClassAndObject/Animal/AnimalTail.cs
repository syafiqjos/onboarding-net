namespace ClassAndObject.Animal {
	public class AnimalTail {
		public int Length { get; }

		public AnimalTail(int length) {
			Length = length;
		}
	}
}
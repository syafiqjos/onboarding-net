let $authorListElement = $("#author_table_list");
let $authorCreateFormElement = $("#author_create_form");
let $authorCreateFormNameFieldElement = $(
	"#author_create_form input[name=author_name]"
);

let authorsContext = {};

const showAuthorDetails = (author) => {
	let content = document.createElement("div");
	content.classList.add("container");
	content.innerHTML = `
                            <div class="row mb-2">
                                <div class="col">ID</div>
                                <div class="col">Name</div>
                            </div>
                            <div class="row">
                                <div class="col">${author.id}</div>
                                <div class="col">${author.name}</div>
                            </div>`;

	swal({
		title: "Author Details",
		content: {
			element: "div",
			attributes: {
				class: "container",
			},
		},
		content: content,
		buttons: {
			cancel: "Close",
		},
	});
};

const updateAuthorPrompt = (author) => {
	swal({
		title: "Update Author name",
		content: {
			element: "input",
			attributes: {
				placeholder: "New author name",
				type: "text",
				value: author.name,
			},
		},
		buttons: {
			cancel: {
				text: "Cancel",
			},
			confirm: {
				text: "Update",
			},
		},
	}).then((newAuthorName) => {
		if (newAuthorName == null) return;
		updateAuthor({ ...author, name: newAuthorName });
	});
};

const updateAuthor = (author) => {
	console.log(author);

	let postData = {
		// id: author.id,
		name: author.name,
	};

	$.ajax({
		type: "PUT",
		url: `https://localhost:5001/api/author/${author.id}`,
		data: JSON.stringify(postData),
		contentType: "application/json",
		success: ({ status, message, response }) => {
			if (status != "success") return;

			swal({
				title: "Alright then, success",
				text: `Author "${author.name}" updated successfully.`,
				icon: "success",
			});

			refreshAuthors();
		},
	});
};

const deleteAuthorWarning = (author) => {
	swal({
		title: "Are you sure?",
		text: `You sure want to delete Author "${author.name}"?`,
		icon: "warning",
		buttons: true,
		dangerMode: true,
	}).then((willDelete) => {
		if (willDelete) {
			deleteAuthor(author);
		}
	});
};

const deleteAuthor = (author) => {
	$.ajax({
		type: "DELETE",
		url: `https://localhost:5001/api/author/${author.id}`,
		success: ({ status, message, response }) => {
			if (status != "success") return;

			swal({
				title: "Alright then, success",
				text: `Author "${author.name}" deleted successfully.`,
				icon: "success",
			});

			refreshAuthors();
		},
	});
};

const addAuthor = (form) => {
	// console.log(form);
	// console.log($authorCreateFormNameFieldElement);

	let authorField = $authorCreateFormNameFieldElement.get(0);
	let authorName = authorField.value;
	let postData = {
		name: authorName,
	};

	$.ajax({
		type: "POST",
		url: "https://localhost:5001/api/author",
		data: JSON.stringify(postData),
		contentType: "application/json",
		success: ({ status, message, response }) => {
			if (status != "success") return;
			let author = response.data;

			refreshAuthors();

			swal(
				"New Author Created!",
				`Author named "${author.name}" successfully added.`,
				"success"
			);

			authorField.value = "";
		},
		error: (e) => {
			console.log(e);
		},
	});
};

const refreshAuthors = () => {
	$.ajax({
		type: "GET",
		url: "https://localhost:5001/api/author",
		success: ({ status, message, response }) => {
			if (status != "success") return;

			let authors = response.data;
			authorsContext = authors;

			let authorsHtml = authors.map((author) => {
				let buttons = `<div class="d-flex flex-row">
                    <button class="btn btn-info mr-2" data-author-id="${author.id}" data-author-name="${author.name}" data-button="details">Details</button>
                    <button class="btn btn-warning mr-2 text-white" data-author-id="${author.id}" data-author-name="${author.name}" data-button="edit">Edit</button>
                    <button class="btn btn-danger mr-2" data-author-id="${author.id}" data-author-name="${author.name}" data-button="delete">Delete</button>
                </div>
                `;

				return `<tr>
                    <td>${author.id}</td>
                    <td>${author.name}</td>
                    <td>${buttons}</td>
                </tr>`;
			});

			$authorListElement.get(0).innerHTML = authorsHtml.join("");

			$("button[data-author-id]").on("click", function () {
				let authorId = this.getAttribute("data-author-id");
				let authorName = this.getAttribute("data-author-name");
				let authorAction = this.getAttribute("data-button");

				if (authorAction == "details") {
					showAuthorDetails({
						id: authorId,
						name: authorName,
					});
				} else if (authorAction == "edit") {
					updateAuthorPrompt({
						id: authorId,
						name: authorName,
					});
				} else if (authorAction == "delete") {
					deleteAuthorWarning({
						id: authorId,
						name: authorName,
					});
				}
			});
		},
		error: () => {
			console.log("Went wrong");
		},
	});
};

$(function () {
	refreshAuthors();
	$authorCreateFormElement.submit(function (e) {
		e.preventDefault();
		addAuthor(this);
	});
});

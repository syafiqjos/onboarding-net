using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace App.Web.Services
{
    public interface IAuthenticationService : IService<IdentityUser>
    {
        Task<IdentityUser> RegisterUser(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, string email, string password);

        Task<IdentityUser> LoginUser(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, string email, string password);

        Task<IdentityUser> GetUser(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager);

        Task LogoutUser(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager);
    }
}
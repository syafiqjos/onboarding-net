using System.Collections.Generic;
using App.DataAccess;
using App.DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Services
{
    public abstract class ServiceBase<TModel> : IService<TModel> where TModel : class
    {
        #region CRUD Service
        public async Task<List<TModel>> GetAll(ApplicationDbContext db)
        {
            return await GetModelDbSet(db).ToListAsync();
        }

        public async Task<TModel> GetById(ApplicationDbContext db, int? id)
        {
            if (id == null) return null;

            return await GetModelDbSet(db).FindAsync(id);
        }

        public async Task Create(ApplicationDbContext db, TModel model)
        {
            await GetModelDbSet(db).AddAsync(model);
            await db.SaveChangesAsync();
        }

        public async Task DeleteById(ApplicationDbContext db, int? id)
        {
            if (id == null) return;

            TModel existingModel = await GetById(db, id);

            if (existingModel == null) return;

            GetModelDbSet(db).Remove(existingModel);
            await db.SaveChangesAsync();
        }

        public async Task UpdateById(ApplicationDbContext db, int? id, TModel newModel)
        {
            if (id == null) return;

            TModel existingModel = await GetById(db, id);

            if (existingModel == null) return;

            AlterModel(existingModel, newModel);

            await db.SaveChangesAsync();
        }
        #endregion

        protected virtual void AlterModel(TModel destination, TModel source)
        {
            /* Example Use
                destination.Name = source.Name;
                destination.Email = source.Email;
            */
        }

        protected virtual DbSet<TModel> GetModelDbSet(ApplicationDbContext db)
        {
            /* Example Use
                return db.Authors;
            */
            return null;
        }
    }
}
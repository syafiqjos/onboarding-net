using System.Collections.Generic;
using App.DataAccess;
using App.DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Services
{
    public class BookService : ServiceBase<Book>, IBookService
    {
        protected override void AlterModel(Book destination, Book source)
        {
            destination.Title = source.Title;
            destination.Description = source.Description;
        }

        protected override DbSet<Book> GetModelDbSet(ApplicationDbContext db)
        {
            return db.Books;
        }
    }
}
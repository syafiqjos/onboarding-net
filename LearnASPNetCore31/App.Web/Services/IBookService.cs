using System.Collections.Generic;
using App.DataAccess;
using App.DataAccess.Model;
using System.Threading.Tasks;

namespace App.Web.Services
{
    public interface IBookService : IService<Book>
    {

    }
}
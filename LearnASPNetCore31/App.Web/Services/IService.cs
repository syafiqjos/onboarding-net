using System.Collections.Generic;
using App.DataAccess;
using App.DataAccess.Model;
using System.Threading.Tasks;

namespace App.Web.Services
{
    public interface IService<TModel>
    {
        Task<List<TModel>> GetAll(ApplicationDbContext db);

        Task<TModel> GetById(ApplicationDbContext db, int? id);

        Task Create(ApplicationDbContext db, TModel author);

        Task DeleteById(ApplicationDbContext db, int? id);

        Task UpdateById(ApplicationDbContext db, int? id, TModel newAuthor);
    }
}
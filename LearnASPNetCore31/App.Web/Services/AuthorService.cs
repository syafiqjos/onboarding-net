using System.Collections.Generic;
using App.DataAccess;
using App.DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Services
{
    public class AuthorService : ServiceBase<Author>, IAuthorService
    {
        protected override void AlterModel(Author destination, Author source)
        {
            destination.Name = source.Name;
        }

        protected override DbSet<Author> GetModelDbSet(ApplicationDbContext db)
        {
            return db.Authors;
        }
    }
}
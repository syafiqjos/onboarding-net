using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using App.DataAccess;
using System.Threading.Tasks;
using System.Web;

namespace App.Web.Services
{
    public class AuthenticationService : ServiceBase<IdentityUser>, IAuthenticationService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AuthenticationService(IHttpContextAccessor httpContextAccessor) : base()
        {
            _httpContextAccessor = httpContextAccessor;
        }

        protected override void AlterModel(IdentityUser destination, IdentityUser source)
        {
            // destination.Title = source.Title;
            // destination.Description = source.Description;
        }

        protected override DbSet<IdentityUser> GetModelDbSet(ApplicationDbContext db)
        {
            return db.Users;
        }

        public async Task<IdentityUser> RegisterUser(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, string email, string password)
        {
            var user = new IdentityUser
            {
                Email = email,
                UserName = email,
            };
            var result = await userManager.CreateAsync(user, password);
            if (!result.Succeeded)
            {
                return null;
            }
            return user;
        }

        public async Task<IdentityUser> LoginUser(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, string email, string password)
        {
            var existingUser = await userManager.FindByEmailAsync(email);
            var result = await signInManager.PasswordSignInAsync(existingUser, password, false, false);

            if (!result.Succeeded)
            {
                return null;
            }

            var user = await GetUser(signInManager, userManager);

            return user;
        }

        public async Task<IdentityUser> GetUser(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        {
            var userContext = _httpContextAccessor.HttpContext.User;
            var user = await userManager.GetUserAsync(userContext);

            return user;
        }

        public async Task LogoutUser(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        {
            await signInManager.SignOutAsync();
        }
    }
}
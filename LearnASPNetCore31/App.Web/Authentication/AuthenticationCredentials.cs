using System.ComponentModel.DataAnnotations;

namespace App.Web.Authentication
{
    public class AuthenticationCredentials
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}

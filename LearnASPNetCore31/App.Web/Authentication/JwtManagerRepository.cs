using System.Linq;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using App.DataAccess.Model;
using Microsoft.Extensions.Configuration;
namespace App.Web.Authentication

{
    public class JwtManagerRepository : JwtManagerRepositoryBase<AuthenticationPayload>
    {
        public JwtManagerRepository(IConfiguration configuration) : base(configuration) { }

        protected override void AlterPayloadDefinition(AuthenticationPayload payload, JwtSecurityToken token)
        {
            payload.Email = token.Payload["Email"] as string;
            payload.FirstName = token.Payload["FirstName"] as string;
            payload.LastName = token.Payload["LastName"] as string;
            payload.FullName = token.Payload["FullName"] as string;

            payload.Nbf = token.Payload.Nbf;
            payload.Exp = token.Payload.Exp;
            payload.Iat = token.Payload.Iat;
        }

        protected override ClaimsIdentity MakeClaimsIdentity(UserDetails user)
        {
            return new ClaimsIdentity(
                new Claim[] {
                    new Claim("Email", user.Email),
                    new Claim("FirstName", user.FirstName),
                    new Claim("LastName", user.LastName),
                    new Claim("FullName", user.FullName)
                }
            );
        }
    }
}
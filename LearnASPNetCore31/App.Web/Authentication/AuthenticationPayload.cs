namespace App.Web.Authentication
{
    public class AuthenticationPayload
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }

        public int? Nbf { get; set; }
        public int? Exp { get; set; }
        public int? Iat { get; set; }
    }
}
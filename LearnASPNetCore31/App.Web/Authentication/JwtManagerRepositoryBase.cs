using App.DataAccess.Model;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Security.Claims;
using System;

namespace App.Web.Authentication
{
    public abstract class JwtManagerRepositoryBase<TPayload> : IJwtManagerRepository<TPayload> where TPayload : new()
    {
        private readonly IConfiguration _configuration;
        private readonly JwtSecurityTokenHandler tokenHandler;
        private readonly byte[] tokenKey;

        public JwtManagerRepositoryBase(IConfiguration configuration)
        {
            _configuration = configuration;

            tokenHandler = new JwtSecurityTokenHandler();
            tokenKey = Encoding.UTF8.GetBytes(_configuration["JWT:Key"]);
        }

        private SecurityTokenDescriptor MakeTokenDescriptor(UserDetails user)
        {
            return new SecurityTokenDescriptor
            {
                Subject = MakeClaimsIdentity(user),
                Expires = DateTime.UtcNow.AddMinutes(10),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(tokenKey), SecurityAlgorithms.HmacSha256Signature)
            };
        }

        public Tokens Authenticate(UserDetails user)
        {
            // Check Login
            return new Tokens();
        }

        public Tokens CreateToken(UserDetails user)
        {
            var tokenDescriptor = MakeTokenDescriptor(user);

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new Tokens
            {
                Token = tokenHandler.WriteToken(token)
            };
        }

        public TPayload ParseToken(string tokenString)
        {
            var token = tokenHandler.ReadJwtToken(tokenString);
            var payload = new TPayload();

            AlterPayloadDefinition(payload, token);

            return payload;
        }

        protected virtual void AlterPayloadDefinition(TPayload payload, JwtSecurityToken token)
        {
            // Set definition here
            /*
                    payload.Email = token.payload.Email
            */
        }

        protected virtual ClaimsIdentity MakeClaimsIdentity(UserDetails user)
        {
            // Set definition here
            /*
                    return new ClaimsIdentity(new Claim[] {
                            new Claim(ClaimTypes.Email, user.Email)
                        }
                    );
            */
            return null;
        }
    }
}

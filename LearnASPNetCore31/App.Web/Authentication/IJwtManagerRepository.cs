using System.Security.Claims;
using App.DataAccess.Model;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace App.Web.Authentication
{
    public interface IJwtManagerRepository<TPayload> where TPayload : new()
    {
        Tokens Authenticate(UserDetails user);
        Tokens CreateToken(UserDetails user);
        TPayload ParseToken(string token);
    }
}

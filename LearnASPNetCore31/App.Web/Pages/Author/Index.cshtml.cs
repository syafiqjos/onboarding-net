using System.Collections.Generic;
using App.DataAccess;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using App.Web.Services;

using AuthorEntity = App.DataAccess.Model.Author;

namespace App.Web.Pages.Author
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _db;
        private readonly IAuthorService _authorService;

        public IndexModel(ApplicationDbContext db, IAuthorService authorService)
        {
            _db = db;
            _authorService = authorService;
        }

        public List<AuthorEntity> Authors { get; set; }

        public async Task OnGet()
        {
            Authors = await _authorService.GetAll(_db);
        }

        /*
            id: AuthorId
        */
        public async Task<IActionResult> OnPostDelete(int id)
        {
            AuthorEntity existingAuthor = await _authorService.GetById(_db, id);
            if (existingAuthor == null) return NotFound();

            await _authorService.DeleteById(_db, id);

            return Redirect("/Author");
        }
    }
}

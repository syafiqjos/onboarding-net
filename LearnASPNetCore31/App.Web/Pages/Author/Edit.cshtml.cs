using App.DataAccess;
using App.DataAccess.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using App.Web.Services;

using AuthorEntity = App.DataAccess.Model.Author;

namespace App.Web.Pages.Author
{
    public class EditModel : PageModel
    {
        private readonly ApplicationDbContext _db;
        private readonly IAuthorService _authorService;

        public EditModel(ApplicationDbContext db, IAuthorService authorService)
        {
            _db = db;
            _authorService = authorService;
        }

        [BindProperty]
        public AuthorEntity Author { get; set; }
        public int AuthorId { get; set; }

        /*
            id: AuthorId
        */
        public async Task<IActionResult> OnGet(int? id)
        {
            if (id == null) return Unauthorized();

            Author = await _authorService.GetById(_db, id);

            if (Author == null) return NotFound();

            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            if (!ModelState.IsValid) return Page();

            if (Author == null) return Unauthorized();

            AuthorEntity ExistingAuthor = await _authorService.GetById(_db, Author.Id);

            if (ExistingAuthor == null) return NotFound();

            await _authorService.UpdateById(_db, Author.Id, Author);

            return Redirect($"/Author/Show?id={Author.Id}");
        }
    }
}

using App.DataAccess;
using App.DataAccess.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;
using App.Web.Services;

using AuthorEntity = App.DataAccess.Model.Author;

namespace App.Web.Pages.Author
{
    public class CreateModel : PageModel
    {
        private readonly ApplicationDbContext _db;
        private readonly IAuthorService _authorService;

        public CreateModel(ApplicationDbContext db, IAuthorService authorService)
        {
            _db = db;
            _authorService = authorService;
        }

        [BindProperty]
        public AuthorEntity Author { get; set; }

        public IActionResult OnGet()
        {
            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            if (!ModelState.IsValid) return Page();

            await _authorService.Create(_db, Author);

            return Redirect($"/Author/Show?id={Author.Id}");
        }
    }
}

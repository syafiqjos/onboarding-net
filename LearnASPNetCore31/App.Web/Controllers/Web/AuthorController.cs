using System;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using App.DataAccess;
using App.DataAccess.Model;

using App.Web.Services;

namespace App.Web.Controllers.Web
{
    [Route("web/[controller]")]
    public class AuthorController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IAuthorService _authorService;

        public AuthorController(ApplicationDbContext db, IAuthorService authorService)
        {
            _db = db;
            _authorService = authorService;
        }

        [Route("index")]
        [Route("")]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var authors = await _authorService.GetAll(_db);

            ViewBag.Authors = authors;

            return View();
        }

        [Route("{id}/details")]
        [Route("{id}")]
        [HttpGet]
        public async Task<IActionResult> Show(int id)
        {
            var author = await _authorService.GetById(_db, id);

            ViewBag.Author = author;

            return View();
        }

        [Route("{id}/edit")]
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var author = await _authorService.GetById(_db, id);

            ViewBag.Author = author;

            return View();
        }

        [Route("{id}/edit")]
        [HttpPost]
        public async Task<IActionResult> Update(int id, Author author)
        {
            await _authorService.UpdateById(_db, id, author);

            return RedirectToAction("Show", new { id = id });
        }

        [Route("{id}/destroy")]
        [HttpPost]
        public async Task<IActionResult> Destroy(int id)
        {
            await _authorService.DeleteById(_db, id);

            return RedirectToAction("Index");
        }

        [Route("create")]
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(Author author)
        {
            await _authorService.Create(_db, author);

            return RedirectToAction("Index");
        }
    }
}
using System;
using System.Threading.Tasks;
using System.Security.Claims;

using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

using App.DataAccess;
using App.DataAccess.Model;
using App.Web.Services;
using App.Web.Authentication;

namespace App.Web.Controllers.Api
{
    // [ApiController()]
    [Route("api/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IJwtManagerRepository<AuthenticationPayload> _jwtManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(
            IJwtManagerRepository<AuthenticationPayload> jwtManager,
            SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            IAuthenticationService authenticationService
        )
        {
            _jwtManager = jwtManager;
            _signInManager = signInManager;
            _userManager = userManager;
            _authenticationService = authenticationService;
        }

        [Route("create")]
        [HttpPost]
        public IActionResult CreateToken(UserDetails userDetails)
        {
            var token = _jwtManager.CreateToken(userDetails);
            return Ok(token);
        }

        [Route("check")]
        [HttpPost]
        public IActionResult CheckToken(string token)
        {
            var authPayload = _jwtManager.ParseToken(token);
            return Ok(authPayload);
        }

        [Route("bearer")]
        [HttpPost]
        [Authorize]
        public IActionResult CheckBearerToken()
        {
            string bearer = Request.Headers["Authorization"];
            string[] bearerStrings = bearer.Split(' ');
            if (bearerStrings.Length < 2) return Unauthorized();

            string bearerToken = bearerStrings[1];
            var authPayload = _jwtManager.ParseToken(bearerToken);

            return Ok(authPayload);
        }

        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> RegisterUser(AuthenticationCredentials credentials)
        {
            var user = await _authenticationService.RegisterUser(_signInManager, _userManager, credentials.Email, credentials.Password);

            if (user == null)
            {
                return BadRequest();
            }

            return Ok(user);
        }

        [Route("login")]
        [HttpPost]
        public async Task<IActionResult> LoginUser(AuthenticationCredentials credentials)
        {
            var user = await _authenticationService.LoginUser(_signInManager, _userManager, credentials.Email, credentials.Password);

            if (user == null) return BadRequest();

            return Ok(user);
        }

        [Route("get")]
        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            var user = await _authenticationService.GetUser(_signInManager, _userManager);

            if (user == null) return BadRequest();

            return Ok(user);
        }

        [Route("logout")]
        [HttpGet]
        public async Task<IActionResult> LogoutUser()
        {
            await _authenticationService.LogoutUser(_signInManager, _userManager);

            return Ok();
        }
    }
}
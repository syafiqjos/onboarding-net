using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

using App.DataAccess;
using App.DataAccess.Model;
using App.Web.Services;
using App.Web.Cache;

using Newtonsoft.Json;

namespace App.Web.Controllers.Api
{
    // [ApiController()]
    [Route("api/[controller]")]
    public class AuthorController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly IAuthorService _authorService;
        // private readonly IMemoryCache _memoryCache;
        private readonly IMemoryCacheManager _memoryCacheManager;
        private readonly IContentCacheManager _contentCacheManager;

        public AuthorController(ApplicationDbContext db, IAuthorService authorService, IMemoryCacheManager memoryCacheManager, IContentCacheManager contentCacheManager)
        {
            _db = db;
            _authorService = authorService;
            _memoryCacheManager = memoryCacheManager;
            _contentCacheManager = contentCacheManager;
        }

        [HttpGet("get-all-cache-memory")]
        public async Task<IActionResult> GetAllCache()
        {
            string cacheKey = "GetAllAuthors";
            List<Author> authors;

            if (!_memoryCacheManager.TryGetCache<List<Author>>(cacheKey, out authors))
            {
                Thread.Sleep(10 * 1000);

                authors = await _authorService.GetAll(_db);
                _memoryCacheManager.SetCache<List<Author>>(cacheKey, authors);
            }

            return Ok(new
            {
                status = "success",
                message = "Authors Retrieved Successfully.",
                response = new
                {
                    errors = new object[] { },
                    data = authors
                }
            });
        }

        [HttpGet("get-all-cache-content")]
        public async Task<IActionResult> GetAllCacheContent()
        {
            string cacheFilename = "GetAllAuthors.json";
            string authorsJson;
            List<Author> authors;

            if (!_contentCacheManager.TryGetCacheAsText(cacheFilename, out authorsJson))
            {
                Thread.Sleep(10 * 1000);

                authors = await _authorService.GetAll(_db);
                authorsJson = JsonConvert.SerializeObject(authors);

                _contentCacheManager.SetCacheAsText(cacheFilename, authorsJson);
            }
            else
            {
                try
                {
                    authors = JsonConvert.DeserializeObject<List<Author>>(authorsJson);
                }
                catch (JsonReaderException)
                {
                    Thread.Sleep(10 * 1000);

                    authors = await _authorService.GetAll(_db);
                    authorsJson = JsonConvert.SerializeObject(authors);

                    _contentCacheManager.SetCacheAsText(cacheFilename, authorsJson);
                }
            }

            return Ok(new
            {
                status = "success",
                message = "Authors Retrieved Successfully.",
                response = new
                {
                    errors = new object[] { },
                    data = authors
                }
            });
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            var authors = await _authorService.GetAll(_db);

            return Ok(new
            {
                status = "success",
                message = "Authors Retrieved Successfully.",
                response = new
                {
                    errors = new object[] { },
                    data = authors
                }
            });
        }

        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] Author author)
        {
            if (!ModelState.IsValid) return BadRequest(new
            {
                status = "fail",
                message = "Author Validation Invalid.",
                response = new
                {
                    errors = ModelState,
                    data = new { }
                }
            });

            await _authorService.Create(_db, author);

            return CreatedAtAction(nameof(Get), new { id = author.Id }, new
            {
                status = "success",
                message = "Author Created.",
                response = new
                {
                    errors = new object[] { },
                    data = author
                }
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var author = await _authorService.GetById(_db, id);

            if (author == null) return NotFound(new
            {
                status = "fail",
                message = $"Author with id: {id} not found.",
                response = new
                {
                    errors = new object[] { },
                    data = new { }
                }
            });

            return Ok(new
            {
                status = "success",
                message = $"Author Retrieved Successfully.",
                response = new
                {
                    errors = new object[] { },
                    data = author
                }
            });
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Author author)
        {
            Author existingAuthor = await _authorService.GetById(_db, id);

            if (existingAuthor == null) return NotFound(new
            {
                status = "fail",
                message = $"Author with id: {id} not found.",
                response = new
                {
                    errors = new object[] { },
                    data = new { }
                }
            });

            if (!ModelState.IsValid) return BadRequest(new
            {
                status = "fail",
                message = "Author Validation Invalid.",
                response = new
                {
                    errors = ModelState,
                    data = new { }
                }
            });

            await _authorService.UpdateById(_db, id, author);

            return Ok(new
            {
                status = "success",
                message = "Author Updated Successfully.",
                response = new
                {
                    errors = new object[] { },
                    data = existingAuthor
                }
            });
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var author = await _authorService.GetById(_db, id);

            if (author == null) return NotFound(new
            {
                status = "fail",
                message = $"Author with id: {id} not found.",
                response = new
                {
                    errors = new object[] { },
                    data = new { }
                }
            });

            await _authorService.DeleteById(_db, id);

            return Ok(new
            {
                status = "success",
                message = $"Author Deleted Successfully.",
                response = new
                {
                    errors = new object[] { },
                    data = new { }
                }
            });
        }
    }
}
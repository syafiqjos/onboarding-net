using Microsoft.Extensions.Caching.Memory;

namespace App.Web.Cache
{
    public interface IMemoryCacheManager
    {
        IMemoryCache GetMemoryCache();
        bool TryGetCache<TModel>(string key, out TModel referencedValue);
        void SetCache<TModel>(string key, TModel value);
    }
}
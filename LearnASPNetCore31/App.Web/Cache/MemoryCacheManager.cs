using Microsoft.Extensions.Caching.Memory;

namespace App.Web.Cache
{
    public class MemoryCacheManager : IMemoryCacheManager
    {
        private readonly IMemoryCache _memoryCache;
        private readonly MemoryCacheEntryOptions _memoryCacheOptions;

        public MemoryCacheManager(IMemoryCache memoryCache, MemoryCacheOptions memoryCacheOptions)
        {
            _memoryCache = memoryCache;
            _memoryCacheOptions = memoryCacheOptions;
        }

        public bool TryGetCache<TModel>(string key, out TModel referencedValue)
        {
            return _memoryCache.TryGetValue<TModel>(key, out referencedValue);
        }

        public void SetCache<TModel>(string key, TModel value)
        {
            _memoryCache.Set<TModel>(key, value, _memoryCacheOptions);
        }

        public IMemoryCache GetMemoryCache()
        {
            return _memoryCache;
        }
    }
}
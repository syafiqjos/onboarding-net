namespace App.Web.Cache
{
    public interface IContentCacheManager
    {
        bool TryGetCacheAsBytes(string filename, out byte[] referencedContent);
        bool TryGetCacheAsText(string filename, out string referencedContent);

        bool SetCacheAsBytes(string filename, byte[] content, bool overwriteIfExists = true);
        bool SetCacheAsText(string filename, string content, bool overwriteIfExists = true);
    }
}
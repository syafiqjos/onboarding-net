using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace App.Web.Cache
{
    public class ContentCacheManager : IContentCacheManager
    {
        private readonly string _cacheDirectory;

        public ContentCacheManager(string cacheDirectory)
        {
            _cacheDirectory = cacheDirectory;
        }

        private string GetCacheFilename(string filename)
        {
            return Path.Join(_cacheDirectory, filename);
        }

        public bool TryGetCacheAsBytes(string filename, out byte[] referencedContent)
        {
            string cacheFilename = GetCacheFilename(filename);

            if (File.Exists(cacheFilename))
            {
                referencedContent = File.ReadAllBytes(cacheFilename);
                return true;
            }

            referencedContent = null;
            return false;
        }

        public bool TryGetCacheAsText(string filename, out string referencedContent)
        {
            string cacheFilename = GetCacheFilename(filename);

            if (File.Exists(cacheFilename))
            {
                referencedContent = File.ReadAllText(cacheFilename);
                return true;
            }

            referencedContent = null;
            return false;
        }

        public bool SetCacheAsBytes(string filename, byte[] content, bool overwriteIfExists = true)
        {
            string cacheFilename = GetCacheFilename(filename);

            if (Directory.Exists(_cacheDirectory))
            {
                if (overwriteIfExists || (!File.Exists(cacheFilename)))
                {
                    File.WriteAllBytes(GetCacheFilename(filename), content);
                    return true;
                }
            }
            return false;
        }

        public bool SetCacheAsText(string filename, string content, bool overwriteIfExists = true)
        {
            string cacheFilename = GetCacheFilename(filename);

            if (Directory.Exists(_cacheDirectory))
            {
                if (overwriteIfExists || (!File.Exists(cacheFilename)))
                {
                    File.WriteAllText(GetCacheFilename(filename), content);
                    return true;
                }
            }
            return false;
        }
    }
}
using System;
using Microsoft.Extensions.Caching.Memory;

namespace App.Web.Cache
{
    public class MemoryCacheOptions : MemoryCacheEntryOptions
    {
        public MemoryCacheOptions()
        {
            Priority = CacheItemPriority.High;
            SlidingExpiration = TimeSpan.FromSeconds(20);
            AbsoluteExpiration = DateTime.Now.AddSeconds(50);
        }
    }
}
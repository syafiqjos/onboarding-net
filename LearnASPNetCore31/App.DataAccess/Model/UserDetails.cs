using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace App.DataAccess.Model
{
    public class UserDetails
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public string FullName { get { return $"{FirstName} {LastName}"; } }

        [Required]
        public IdentityUser User { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace App.DataAccess.Model
{
    public class Book
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }

        [Required]
        public Author Author { get; set; }
    }
}

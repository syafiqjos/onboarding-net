﻿using System.ComponentModel.DataAnnotations;

namespace App.DataAccess.Model
{
    public class Author
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
